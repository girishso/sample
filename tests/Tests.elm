module Tests exposing (..)

import App exposing (..)
import Expect
import Fuzz exposing (Fuzzer, int, list, string)
import String
import Test exposing (..)


validate : Test
validate =
    let
        model =
            App.init ""
                |> Tuple.first
    in
    describe "validate"
        [ test "empty q is not allowed" <|
            \() ->
                Expect.equal (Err "q: is Blank") (App.validate { model | q = "" })
        , test "* not allowed" <|
            \() ->
                Expect.equal (Err "q: * Not allowed") (App.validate { model | q = "iphone*" })
        , test "currency must if price is set" <|
            \() ->
                Expect.equal (Err "currency: must be selected")
                    (App.validate
                        { model
                            | q = "iphone"
                            , priceCurrency = ( Min 1, NoCurrency )
                        }
                    )
        ]


createUrl : Test
createUrl =
    let
        model =
            App.init ""
                |> Tuple.first
    in
    describe "createUrl"
        [ test "only q" <|
            \() ->
                Expect.equal "/search?q=iphone" (App.createUrl { model | q = "iphone" })
        , test "Min price and Currency" <|
            \() ->
                Expect.equal "/search?q=iphone&filter=price:[20],priceCurrency:USD"
                    (App.createUrl
                        { model
                            | q = "iphone"
                            , priceCurrency = ( Min 20, USD )
                        }
                    )
        , test "only currency" <|
            \() ->
                Expect.equal "/search?q=iphone&filter=priceCurrency:USD"
                    (App.createUrl
                        { model
                            | q = "iphone"
                            , priceCurrency = ( NoPrice, USD )
                        }
                    )
        , test "min max seller type" <|
            \() ->
                Expect.equal "/search?q=iphone&filter=price:[20..50],priceCurrency:EUR,sellerAccountTypes:{Individual}"
                    (App.createUrl
                        { model
                            | q = "iphone"
                            , priceCurrency = ( MinMax ( 20, 50 ), EUR )
                            , sellerAccountType = Individual
                        }
                    )
        , test "only max" <|
            \() ->
                Expect.equal "/search?q=iphone&filter=price:[..50],priceCurrency:EUR"
                    (App.createUrl
                        { model
                            | q = "iphone"
                            , priceCurrency = ( Max 50, EUR )
                        }
                    )
        ]
