module App exposing (..)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)


---- MODEL ----


type alias Model =
    { q : String
    , priceCurrency : ( Price, Currency )
    , sellerAccountType : SellerAccountType
    , url : Result String String
    }


type Currency
    = USD
    | EUR
    | GBP
    | CAD
    | NoCurrency


type Price
    = MinMax ( Int, Int )
    | Min Int
    | Max Int
    | NoPrice


type SellerAccountType
    = NoAccount
    | Individual
    | Business


init : String -> ( Model, Cmd Msg )
init path =
    ( { q = "", priceCurrency = ( NoPrice, NoCurrency ), sellerAccountType = NoAccount, url = Ok "" }, Cmd.none )



---- UPDATE ----


type Msg
    = SetQ String
    | SetMinPrice String
    | SetMaxPrice String
    | SetCurrency String
    | SetSellerAccountType String
    | OnSearch


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetQ q ->
            ( { model | q = q }, Cmd.none )

        SetMinPrice price ->
            ( { model | priceCurrency = changeMinprice price model.priceCurrency }, Cmd.none )

        SetMaxPrice price ->
            ( { model | priceCurrency = changeMaxprice price model.priceCurrency }, Cmd.none )

        SetCurrency currency ->
            let
                ( price, _ ) =
                    model.priceCurrency
            in
            ( { model | priceCurrency = ( price, strToCurrency currency ) }, Cmd.none )

        SetSellerAccountType newType ->
            ( { model | sellerAccountType = strToAccountType newType }, Cmd.none )

        OnSearch ->
            case validate model of
                Ok _ ->
                    ( { model | url = Ok (createUrl model) }, Cmd.none )

                Err str ->
                    ( { model | url = Err str }, Cmd.none )


validate : Model -> Result String Bool
validate model =
    if String.contains "*" model.q then
        Err "q: * Not allowed"
    else if isBlank model.q then
        Err "q: is Blank"
    else
        let
            ( price, currency ) =
                model.priceCurrency
        in
        if price /= NoPrice && currency == NoCurrency then
            Err "currency: must be selected"
        else
            Ok True


changeMinprice : String -> ( Price, Currency ) -> ( Price, Currency )
changeMinprice minPrice ( price, currency ) =
    let
        setMin newp =
            case price of
                MinMax ( min, max ) ->
                    MinMax ( newp, max )

                Min _ ->
                    Min newp

                Max max ->
                    MinMax ( newp, max )

                NoPrice ->
                    Min newp

        removeMin =
            case price of
                MinMax ( min, max ) ->
                    Max max

                Min _ ->
                    NoPrice

                Max max ->
                    Max max

                NoPrice ->
                    NoPrice
    in
    minPrice
        |> String.toInt
        |> Result.map (\minPriceInt -> ( setMin minPriceInt, currency ))
        -- remove invalid min
        |> Result.withDefault ( removeMin, currency )


changeMaxprice : String -> ( Price, Currency ) -> ( Price, Currency )
changeMaxprice maxPrice ( price, currency ) =
    let
        setMax newp =
            case price of
                MinMax ( min, max ) ->
                    MinMax ( min, newp )

                Min min ->
                    MinMax ( min, newp )

                Max _ ->
                    Max newp

                NoPrice ->
                    Max newp

        removeMax =
            case price of
                MinMax ( min, max ) ->
                    Min min

                Min min ->
                    Min min

                Max _ ->
                    NoPrice

                NoPrice ->
                    NoPrice
    in
    maxPrice
        |> String.toInt
        |> Result.map
            (\maxPriceInt -> ( setMax maxPriceInt, currency ))
        -- remove invalid max
        |> Result.withDefault ( removeMax, currency )


strToCurrency : String -> Currency
strToCurrency str =
    case str of
        "GBP" ->
            GBP

        "EUR" ->
            EUR

        "CAD" ->
            CAD

        "USD" ->
            USD

        _ ->
            NoCurrency


strToAccountType : String -> SellerAccountType
strToAccountType string =
    case string of
        "Individual" ->
            Individual

        "Business" ->
            Business

        _ ->
            NoAccount


isBlank : String -> Bool
isBlank =
    String.trim >> String.isEmpty



---- VIEW ----


view : Model -> Html Msg
view model =
    section [ class "hero is-fullheight is-default is-bold main" ]
        [ div [ class "container has-text-centered" ]
            [ div [ id "status-wrapper", class "columns" ]
                [ div [ class "column is-two-fifths" ]
                    [ h1 [ class "title" ] [ text "Search Form" ]
                    , renderField "Search:" (onInput SetQ) "text"
                    , renderField "Min Price:" (onInput SetMinPrice) "number"
                    , renderField "Max Price:" (onInput SetMaxPrice) "number"
                    , renderDropDown "Currency:"
                        (onInput SetCurrency)
                        "-- Select currency --"
                        [ viewOption USD, viewOption EUR, viewOption GBP, viewOption CAD ]
                    , renderDropDown "Seller Account Type:"
                        (onInput SetSellerAccountType)
                        "-- Select account type --"
                        [ viewOption Individual, viewOption Business ]
                    , renderButton "Search" (onClick OnSearch) "is-primary"
                    ]
                , div [ class "column" ] []
                ]
            , div [ class "result" ]
                [ code []
                    [ text <|
                        case model.url of
                            Ok string ->
                                string

                            Err string ->
                                "Error: " ++ string
                    ]
                ]
            ]
        ]


renderField : String -> Attribute Msg -> String -> Html Msg
renderField label_ msg fieldType =
    controlField label_ (input [ class "input", msg, type_ fieldType ] [])


controlField : String -> Html msg -> Html msg
controlField label_ control =
    div [ class "field  is-horizontal" ]
        [ div [ class "field-label is-normal" ]
            [ label [ class "label" ] [ text label_ ] ]
        , div [ class "field-body " ]
            [ div [ class "field" ]
                [ div [ class "control" ]
                    [ control ]
                ]
            ]
        ]


renderDropDown : String -> Attribute Msg -> String -> List (Html Msg) -> Html Msg
renderDropDown label_ msg selectLabel opts =
    controlField label_
        (div
            [ class "select" ]
            [ select [ msg ]
                (opts
                    |> List.append [ option [ value "" ] [ text selectLabel ] ]
                )
            ]
        )


renderButton : String -> Attribute Msg -> String -> Html Msg
renderButton label_ msg class_ =
    div [ class "field is-horizontal" ]
        [ div [ class "field-label" ] []
        , div [ class "field-body" ]
            [ div [ class "field" ]
                [ div [ class "control" ] [ button [ msg, class <| "button " ++ class_ ] [ text label_ ] ]
                ]
            ]
        ]


viewOption : a -> Html Msg
viewOption a =
    option
        [ value <| toString a ]
        [ text <| toString a ]


createUrl : Model -> String
createUrl model =
    let
        sellerAccountTypeFilter =
            case model.sellerAccountType of
                NoAccount ->
                    ""

                x ->
                    "sellerAccountTypes:{" ++ toString x ++ "}"

        allFilters =
            let
                ( price, currency ) =
                    model.priceCurrency

                currencyFilter =
                    case currency of
                        NoCurrency ->
                            ""

                        _ ->
                            "priceCurrency:" ++ toString currency

                priceFilter =
                    case price of
                        MinMax ( min, max ) ->
                            "price:[" ++ toString min ++ ".." ++ toString max ++ "]"

                        Min min ->
                            "price:[" ++ toString min ++ "]"

                        Max max ->
                            "price:[.." ++ toString max ++ "]"

                        NoPrice ->
                            ""
            in
            [ priceFilter, currencyFilter, sellerAccountTypeFilter ]

        allFiltersStr =
            allFilters
                |> List.filter (\f -> not (String.isEmpty f))
                |> String.join ","
    in
    "/search?q="
        |> flip String.append model.q
        |> flip String.append
            (if String.isEmpty allFiltersStr then
                ""
             else
                "&filter=" ++ allFiltersStr
            )



---- PROGRAM ----


main : Program String Model Msg
main =
    Html.programWithFlags
        { view = view
        , init = init
        , update = update
        , subscriptions = \_ -> Sub.none
        }
